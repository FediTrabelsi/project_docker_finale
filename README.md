# Project Management Platform

This project is meant to be a platform that allows users to better manage their project in many ways and it provides 3 major services :
The ability to a profile to present yourself and show what areas you worked or want to work in.
The ability to create a new project , invite other users to joint it or request permission to join an existing one.
An open space to manage the project that provides a space for uploading shared files, documentations .. , create new tasks ,assign them to project members and supervise their progression ..
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You only need to have installed docker into your machine . for opensuse you simpely run :
zypper install docker



### Installing

Navigate to the root folder of the project : the one that contains the file "docker-compose.yml"
open the terminal and run :

```
$docker-compose up
```





## Editing the project

if you want to customize the project by adding deleting or editing some files you have to 
rebuild the containers so your changes could be added so run

Explain what these tests test and why

```
$docker-compose build
```

## Built With

* [AngularJs](https://angularjs.org/) -for front-end
* [NodeJs](https://nodejs.org/en/) -for server side
* [Express](https://expressjs.com/) -as node framework
* [MongoDB](https://www.mongodb.com/) -as a database



## Authors

* **Fedi Trabelsi** - *Student in Eniso* - [Facebook](https://www.facebook.com/fediitrabelsi)


